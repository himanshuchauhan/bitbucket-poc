@RestResource(urlmapping='/CreateAccount/*')
global class AccountController
{
    @HttpPost
    global static String createSingleAccount(String Name)
    {
        Account acc = new Account();
        acc.Name = Name;
        insert acc;
        system.debug('#acc'+acc);
        return acc.id;
    }
    
    @HttpGet
    global static List<Account> returnAccounts()
    {
        RestRequest req = RestContext.request;
        String name = req.params.get('name');
        
        List<Account> accList = [select id, name from account where name = :name];
        
        return accList;
    }
}